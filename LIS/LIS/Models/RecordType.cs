﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIS.Models
{
    public static class RecordType
    {
        public const string HeaderRecord = "a";
        public const string PatientRecord = "c";
        public const string DoctorRecord = "d";
        public const string TestResultRecord = "f";
        public const string TrailerRecord = "h"; // indicates end of sample
    }
}