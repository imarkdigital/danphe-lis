﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIS.Models
{
    public class Result
    {
        public string Test { get; set; }
        public string Value { get; set; }
        public string Unit { get; set; }
        public string ErrorFlag { get; set; }
        public string Error { get; set; }

        public string WarningFlag { get; set; }
        public string Warning { get; set; }

        public int SampleId { get; set; }
        public virtual Sample Sample { get; set; }
        

        public static Dictionary<string, string> ErrorTypes { get; } = new Dictionary<string, string>
        {
            {"0","No error" }
            ,{"1","Above laboratory’s range"}
            ,{"2","Below laboratory’s range"}
            ,{"3","Outside dynamic range"}
            ,{"4","Above analyzer’s range (the value reported is the maximum limit of the range)"}
            ,{"5","Below analyzer’s range (the value reported is the minimum limit of the range)"}
            ,{"6","Prediction failure (floating point test result, starting location 10, becomes 99999.99)"}
            ,{"7","Outside Supplementary Range"}
            ,{"A","Control result is more than 2 SDI from baseline interval mean, but no more than 3 SDI"}
            ,{"B"," Result is more than 3 SDI from baseline interval mean"}
            ,{"C","No baseline interval mean and SDI, or test is not supported in QC database "}
            ,{"D"," Control result below QC range #"}
            ,{"E","Control result above QC range #"}
            ,{"F","Above dynamic range"}
            ,{"G"," Below dynamic range"}
            ,{"H","Above Supplementary Range"}
            ,{"I","Below Supplementary Range "}
        };

        public static Dictionary<string, string> WarningTypes { get; } = new Dictionary<string, string>()
        {
            {"0" ,"No Warning"}
            ,{"1","Analyzer-generated warning" }
            ,{"2","Operator-induced warning" }
            ,{"3","Both analyzer and operator warning" }
        };
    }
}