﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIS.Models
{
    public class Sample
    {
        public Sample()
        {
            Results = new List<Result>();
        }

        public int Id { get; set; }
        public string Analyzer { get; set; }
        //public TimeSpan Time { get; set; }
        public DateTime DateTime { get; set; }
        public string SampleID { get; set; }

        public FluidType Fluid { get; set; }
        public string Quadrant { get; set; }
        public string Cup { get; set; }
        public string TrayName { get; set; }
        public string StatFlag { get; set; }
        public string ControlFlag { get; set; }
        public Mode Mode { get; set; }
        public string DilutionFactor { get; set; }

        public virtual ICollection<Result> Results { get; set; }
        
    }
}