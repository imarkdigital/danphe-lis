﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIS.Models
{
    public enum FluidType
    {
        Serum = 1,
        CSF = 2, //Cerebrospinal fluid(CSF)
        Urine = 3
    }

    public enum Mode
    {
        Select = 0,
        Batch = 1
    }
}