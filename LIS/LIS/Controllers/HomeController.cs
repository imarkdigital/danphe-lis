﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LIS.Models;

namespace LIS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            ////string message = "!002fALB     3.2 g/dL    0061";
            //string filePath = @"C:\Users\Acer\Desktop\LIS\vitros350message_decoded.txt";
            //IEnumerable<string> reportDataLines = ReadDataFromFile(filePath);
            //int lineWithDataCount = reportDataLines.Count(l => l.Length > 1 && l.Substring(4, 1) == "f");
            //reportDataLines = reportDataLines.Where(l => l.Length > 1);
            //List<VitrosData> dataList = new List<VitrosData>();
            //VitrosData data = new VitrosData();
            //foreach (string line in reportDataLines)
            //{
            //    if (line.Substring(4, 1) == "a")
            //    {
            //        data = new VitrosData();
            //        data.Analyzer = line.Substring(11, 6);
            //        string dateString = line.Substring(23, 6);
            //        data.DateTime = DateTime.ParseExact(dateString, "yyMMdd", CultureInfo.InvariantCulture);
            //        string timeString = line.Substring(17, 6);

            //        data.DateTime = data.DateTime
            //            + new TimeSpan(Convert.ToInt32(timeString.Substring(0, 2)), Convert.ToInt32(timeString.Substring(2, 2)), Convert.ToInt32(timeString.Substring(4, 2)));

            //        switch (line.Substring(44, 1))
            //        {
            //            case "1":
            //                data.Fluid = FluidType.Serum;
            //                break;
            //            case "2":
            //                data.Fluid = FluidType.CSF;
            //                break;
            //            case "3":
            //                data.Fluid = FluidType.Urine;
            //                break;

            //        }
            //        data.Quadrant = line.Substring(44, 1);
            //        data.Cup = line.Substring(46, 2);
            //        //data.TryName = string.IsNullOrEmpty(line.Substring(48, 15)) ? "No Tray Name" : line.Substring(48, 15);
            //        data.TrayName = line.Substring(48, 15).All(c => c.ToString() == "") == true ? "No Tray Nam" : line.Substring(48, 15);
            //        //string TrayName = line.Substring(48, 15);

            //        //bool isTrayName = TrayName.All(c => c.Equals('\0'));
            //        data.StatFlag = line.Substring(63, 1);
            //        data.ControlFlag = line.Substring(64, 1);
            //        data.Mode = line.Substring(65, 1) == "0" ? Mode.Select : Mode.Batch;
            //        data.DilutionFactor = line.Substring(66, 7);
            //        data.SampleID = line.Substring(29, 15);
            //    }
            //    if (line.Substring(4, 1) != "h" && line.Substring(4, 1) == "f")
            //    {

            //        data.Test = line.Substring(5, 4);

            //        data.Result = line.Substring(9, 8);// line.Substring(9, 9)

            //        data.Unit = line.Substring(17, 7);  //line.Substring(18, 8)

            //        data.ErrorFlags = line.Substring(25, 1); //line.Substring(26, 1)

            //        data.WarningFlags = line.Substring(26, 1); //line.Substring(27, 1)
            //        dataList.Add(data);
            //    }

            // }


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }





        public ActionResult LIS()
        {
            //string message = "!002fALB     3.2 g/dL    0061";
            string filePath = @"C:\Users\Acer\Desktop\LIS\vitros350message_decoded.txt";
            IEnumerable<string> reportDataLines = ReadDataFromFile(filePath);
            int lineWithDataCount = reportDataLines.Count(l => l.Length > 1 && l.Substring(4, 1) == "f");
            reportDataLines = reportDataLines.Where(l => l.Length > 1);
            List<VitrosData> dataList = new List<VitrosData>();
            VitrosData data = new VitrosData();
            foreach (string line in reportDataLines)
            {
                if (line.Substring(4, 1) == "a")
                {
                    data = new VitrosData();
                    data.Analyzer = line.Substring(11, 6);
                    string dateString = line.Substring(23, 6);
                    data.DateTime = DateTime.ParseExact(dateString, "yyMMdd", CultureInfo.InvariantCulture);
                    string timeString = line.Substring(17, 6);

                    data.DateTime = data.DateTime
                        + new TimeSpan(Convert.ToInt32(timeString.Substring(0, 2)), Convert.ToInt32(timeString.Substring(2, 2)), Convert.ToInt32(timeString.Substring(4, 2)));

                    switch (line.Substring(44, 1))
                    {
                        case "1":
                            data.Fluid = FluidType.Serum;
                            break;
                        case "2":
                            data.Fluid = FluidType.CSF;
                            break;
                        case "3":
                            data.Fluid = FluidType.Urine;
                            break;

                    }
                    data.Quadrant = line.Substring(44, 1);
                    data.Cup = line.Substring(46, 2);
                    data.TrayName = line.Substring(48, 15).All(c => Char.IsWhiteSpace(c)) == true ? "No Tray Name" : line.Substring(48, 15);
                    //data.TrayName = "Tray Name Not Assigned";


                    data.StatFlag = line.Substring(63, 1);
                    data.ControlFlag = line.Substring(64, 1);
                    data.Mode = line.Substring(65, 1) == "0" ? Mode.Select : Mode.Batch;
                    data.DilutionFactor = line.Substring(66, 7);
                    data.SampleID = line.Substring(29, 15);
                }
                if (line.Substring(4, 1) != "h" && line.Substring(4, 1) == "f")
                {

                    data.Test = line.Substring(5, 4);

                    data.Result = line.Substring(9, 8);// line.Substring(9, 9)

                    data.Unit = line.Substring(17, 7);  //line.Substring(18, 8)

                    data.ErrorFlags = line.Substring(25, 1); //line.Substring(26, 1)
                    data.Error = ErrorFlags.Error.FirstOrDefault(err => err.Key == data.ErrorFlags).Value;

                    data.WarningFlags = line.Substring(26, 1); //line.Substring(27, 1)
                    dataList.Add(data);
                }

            }
            return View(dataList);
        }




        public ActionResult LIS2()
        {
            string filePath = @"C:\Users\Acer\Desktop\LIS\vitros350message_decoded.txt";

            IEnumerable<string> reportDataLines = ReadDataFromFile(filePath);

            IEnumerable<Sample> samples = GetSampleData(reportDataLines);
            samples = RemoveDuplicateSamples(samples);

           
            return View(samples);
        }

        private List<Sample> GetSampleData(IEnumerable<string> reportDataLines)
        {
            List<Sample> samples = new List<Sample>();
            Sample sample = new Sample();
            foreach (string line in reportDataLines.Where(l => l.Length > 1))
            {
                if (line.Substring(4, 1) == RecordType.HeaderRecord)
                {
                    sample = new Sample();
                    sample.Analyzer = line.Substring(11, 6);
                    string date = line.Substring(23, 6);
                    sample.DateTime = DateTime.ParseExact(date, "yyMMdd", CultureInfo.InvariantCulture);
                    string time = line.Substring(17, 6);

                    sample.DateTime += new TimeSpan(Convert.ToInt32(time.Substring(0, 2)), Convert.ToInt32(time.Substring(2, 2)), Convert.ToInt32(time.Substring(4, 2)));

                    switch (line.Substring(44, 1))
                    {
                        case "1":
                            sample.Fluid = FluidType.Serum;
                            break;
                        case "2":
                            sample.Fluid = FluidType.CSF;
                            break;
                        case "3":
                            sample.Fluid = FluidType.Urine;
                            break;
                    }
                    sample.Quadrant = line.Substring(44, 1);
                    sample.Cup = line.Substring(46, 2);
                    sample.TrayName = line.Substring(48, 15).All(c => Char.IsWhiteSpace(c)) == true ? "No Tray Name" : line.Substring(48, 15);
                    sample.StatFlag = line.Substring(63, 1);
                    sample.ControlFlag = line.Substring(64, 1);
                    sample.Mode = line.Substring(65, 1) == "0" ? Mode.Select : Mode.Batch;
                    sample.DilutionFactor = line.Substring(66, 7);
                    sample.SampleID = line.Substring(29, 15);
                    samples.Add(sample);

                }
                if (line.Substring(4, 1) == RecordType.TestResultRecord)
                {
                    Result result = new Result
                    {
                        Test = line.Substring(5, 4),

                        Value = line.Substring(9, 8),// line.Substring(9, 9)

                        Unit = line.Substring(17, 7),  //line.Substring(18, 8)

                        ErrorFlag = line.Substring(25, 1), //line.Substring(26, 1)

                        Error = Result.ErrorTypes.FirstOrDefault(e => e.Key == line.Substring(25, 1)).Value,

                        WarningFlag = line.Substring(26, 1), //line.Substring(27, 1)

                        Warning = Result.WarningTypes.FirstOrDefault(w => w.Key == line.Substring(26, 1)).Value
                    };
                    sample.Results.Add(result);

                }
            }
            return samples;
        }

        private List<Sample> RemoveDuplicateSamples(IEnumerable<Sample> data)
        {
            return data.GroupBy(s => s.SampleID).Select(g => g.FirstOrDefault()).ToList();
        }

        private List<string> ReadDataFromFile(string fileName)
        {
            try
            {
                List<string> listString = new List<string>();
                //string sourceFile = System.IO.Path.Combine(fileName);
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    StreamReader sr = new StreamReader(fs);
                    while (true)
                    {
                        string line = sr.ReadLine();
                        if (line == null)
                            break;
                        listString.Add(line);
                    }
                    sr.Close();
                }

                return listString;
            }
            catch (Exception ex)
            {
                //log error
                return new List<string>();
            }
        }
    }
}